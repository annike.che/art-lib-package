(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('rxjs'), require('lodash'), require('rxjs/operators'), require('@angular/router')) :
    typeof define === 'function' && define.amd ? define('@art-lib/query-params-binder', ['exports', '@angular/core', '@angular/forms', 'rxjs', 'lodash', 'rxjs/operators', '@angular/router'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory((global['art-lib'] = global['art-lib'] || {}, global['art-lib']['query-params-binder'] = {}), global.ng.core, global.ng.forms, global.rxjs, global.lodash, global.rxjs.operators, global.ng.router));
}(this, (function (exports, i0, forms, rxjs, lodash, operators, i1) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    /** @deprecated */
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    /** @deprecated */
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    function __spreadArray(to, from) {
        for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
            to[j] = from[i];
        return to;
    }
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }
    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    function parser(value, typeValue) {
        switch (typeValue) {
            case 'string':
                return value;
            case 'number':
                return +value;
            case 'boolean':
                return value === 'false' ? false : !!value;
            case 'object':
                return JSON.parse(value);
            case 'array<string>':
                return value ? value === null || value === void 0 ? void 0 : value.split(',') : undefined;
            case 'array<number>': {
                // console.log('array<number> value', value, value?.split(','));
                var result = value ? value === null || value === void 0 ? void 0 : value.split(',').map(function (el) { return +el; }) : undefined;
                // console.log('array<number>', result);
                return result;
            }
            default:
                return value;
        }
    }
    function resolveParams(params) {
        params = transformArray(params);
        var result = {};
        params.forEach(function (el) {
            var _a, _b, _c;
            if (el.hasOwnProperty('defValue')) {
                // console.log(el.value, el.defValue);
                // console.log(JSON.stringify(el.value), JSON.stringify(el.defValue));
                var isChange = JSON.stringify(el.value) !== JSON.stringify(el.defValue);
                result[el.queryKey] = isChange ? ((_a = el.value) === null || _a === void 0 ? void 0 : _a.toString()) || '' : null;
            }
            else {
                // console.log('')
                var isEmpty = el.value === null || el.value === undefined || !((_b = el.value) === null || _b === void 0 ? void 0 : _b.toString());
                result[el.queryKey] = isEmpty ? null : ((_c = el.value) === null || _c === void 0 ? void 0 : _c.toString()) || '';
            }
        });
        console.log('result', result);
        return result;
    }
    function transformArray(value) {
        return Array.isArray(value) ? value : [value];
    }

    var QueryParamDef = /** @class */ (function () {
        function QueryParamDef(config) {
            this.config = config;
        }
        Object.defineProperty(QueryParamDef.prototype, "queryKey", {
            get: function () {
                return this.config.queryKey;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(QueryParamDef.prototype, "defValue", {
            get: function () {
                return this.config.defValue || undefined;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(QueryParamDef.prototype, "type", {
            get: function () {
                return this.config.type || 'string';
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(QueryParamDef.prototype, "path", {
            get: function () {
                return this.config.path || this.queryKey;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(QueryParamDef.prototype, "parser", {
            get: function () {
                return this.config.parser;
            },
            enumerable: false,
            configurable: true
        });
        QueryParamDef.prototype.parse = function (queryParamValue) {
            if (this.parser) {
                return this.parser(queryParamValue);
            }
            return parser(queryParamValue, this.type);
        };
        return QueryParamDef;
    }());

    var QueryParamsBinderManager = /** @class */ (function () {
        function QueryParamsBinderManager(router, defs, window) {
            this.router = router;
            this.defs = defs;
            this.window = window;
            this.group = new forms.FormGroup({}); // группа параметров
            this.$destroy = new rxjs.Subject();
            // принимаем конфиг
            this.paramsDef = transformArray(defs).map(function (val) { return new QueryParamDef(val); });
        }
        QueryParamsBinderManager.prototype.connect = function (group) {
            console.log('init group', group);
            this.group = group;
            this.init();
            return this;
        };
        QueryParamsBinderManager.prototype.init = function () {
            var _this = this;
            // обновляем контролы group из url строки
            this.updateControl(this.paramsDef, { emitEvent: true });
            // создаем массив с контролами параметров
            var controls = this.paramsDef.map(function (el) {
                var _a;
                return ((_a = _this.group.get(el.path)) === null || _a === void 0 ? void 0 : _a.valueChanges.pipe(operators.debounceTime(800), operators.distinctUntilChanged(), operators.map(function (value) {
                    //console.log('el', el);
                    var option = {
                        value: value,
                        queryKey: el.queryKey,
                    };
                    if (el.defValue) {
                        option['defValue'] = el.defValue;
                    }
                    return option;
                }), operators.tap(function (value) { console.log('value', value); }))) || new rxjs.Subject();
            });
            /*
            * т.к. routerNavigate - это microtask,
            * нам необходимо агрегировать все изменения при помощи merge.
            * т.к. изменения могут происходить достаточно часто
            * */
            var buffer = [];
            rxjs.merge.apply(void 0, __spread(controls)).pipe(
            // Получаем изменения, кладем в буффер
            operators.map(function (res) {
                console.log('map', res);
                // удаляем повторы
                var index = buffer.findIndex(function (el) { return el.queryKey === res.queryKey; });
                console.log('index', index);
                if (index >= 0) {
                    buffer.splice(index, 1);
                }
                return buffer.push(res);
            }), 
            // Игнорируем какое-то время и выдаем последнее(ние) изменение(ния)
            operators.debounceTime(1000), 
            // Отписываеся, когда срабатывает subject $destroy
            operators.takeUntil(this.$destroy))
                .subscribe(function () {
                console.log('buffer', buffer);
                // из буфера кладем в роутер только непустые параметры || != значению по умолчанию
                _this.updateQueryParams(resolveParams(buffer));
                buffer = [];
            });
        };
        /*
        * Обновление query параметров в route
        * */
        QueryParamsBinderManager.prototype.updateQueryParams = function (params) {
            this.router.navigate([], {
                queryParams: params,
                queryParamsHandling: 'merge',
                replaceUrl: true,
            });
        };
        QueryParamsBinderManager.prototype.updateControl = function (defs, options, updateKey) {
            var e_1, _b;
            if (updateKey === void 0) { updateKey = function () { return true; }; }
            // получаем все параметры из Url строки
            var queryParams = new URLSearchParams(this.window.location.search);
            var changedParams = {};
            try {
                for (var defs_1 = __values(defs), defs_1_1 = defs_1.next(); !defs_1_1.done; defs_1_1 = defs_1.next()) {
                    var item = defs_1_1.value;
                    var queryKey = item.queryKey;
                    // если в URL есть параметр из присланных из модуля
                    if (queryParams.has(queryKey)) {
                        // берем значение из queryParams
                        var newValueFromQuery = queryParams.get(queryKey);
                        // парсим значение в правильный тип и вставляем в value по пути path
                        lodash.set(changedParams, item.path, item.parse(newValueFromQuery));
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (defs_1_1 && !defs_1_1.done && (_b = defs_1.return)) _b.call(defs_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // если есть хотя бы 1 параметр на изменение
            if (Object.keys(changedParams).length) {
                this.group.patchValue(changedParams, options);
                console.log('group', this.group);
                console.log('defs', this.defs, defs);
            }
        };
        QueryParamsBinderManager.prototype.destroy = function () {
            this.$destroy.next();
        };
        return QueryParamsBinderManager;
    }());

    var QueryParamsBinderFactory = /** @class */ (function () {
        function QueryParamsBinderFactory(router) {
            this.router = router;
        }
        QueryParamsBinderFactory.prototype.create = function (params) {
            return new QueryParamsBinderManager(this.router, params, window);
        };
        return QueryParamsBinderFactory;
    }());
    QueryParamsBinderFactory.ɵfac = function QueryParamsBinderFactory_Factory(t) { return new (t || QueryParamsBinderFactory)(i0.ɵɵinject(i1.Router)); };
    QueryParamsBinderFactory.ɵprov = i0.ɵɵdefineInjectable({ token: QueryParamsBinderFactory, factory: QueryParamsBinderFactory.ɵfac, providedIn: 'root' });
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(QueryParamsBinderFactory, [{
                type: i0.Injectable,
                args: [{ providedIn: 'root' }]
            }], function () { return [{ type: i1.Router }]; }, null);
    })();

    /*
     * Public API Surface of query-params-binder
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.QueryParamsBinderFactory = QueryParamsBinderFactory;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=art-lib-query-params-binder.umd.js.map
