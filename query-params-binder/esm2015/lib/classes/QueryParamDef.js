import { parser } from '../helpers/utils';
export class QueryParamDef {
    constructor(config) {
        this.config = config;
    }
    get queryKey() {
        return this.config.queryKey;
    }
    get defValue() {
        return this.config.defValue || undefined;
    }
    get type() {
        return this.config.type || 'string';
    }
    get path() {
        return this.config.path || this.queryKey;
    }
    get parser() {
        return this.config.parser;
    }
    parse(queryParamValue) {
        if (this.parser) {
            return this.parser(queryParamValue);
        }
        return parser(queryParamValue, this.type);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUXVlcnlQYXJhbURlZi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3F1ZXJ5LXBhcmFtcy1iaW5kZXIvc3JjL2xpYi9jbGFzc2VzL1F1ZXJ5UGFyYW1EZWYudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBRXhDLE1BQU0sT0FBTyxhQUFhO0lBRXhCLFlBQXFCLE1BQXNDO1FBQXRDLFdBQU0sR0FBTixNQUFNLENBQWdDO0lBQUcsQ0FBQztJQUcvRCxJQUFJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO0lBQzlCLENBQUM7SUFFRCxJQUFJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLFNBQVMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsSUFBSSxJQUFJO1FBQ04sT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxRQUFRLENBQUM7SUFDdEMsQ0FBQztJQUVELElBQUksSUFBSTtRQUNOLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUMzQyxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ1IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUM1QixDQUFDO0lBRUQsS0FBSyxDQUFDLGVBQXVCO1FBQzNCLElBQUssSUFBSSxDQUFDLE1BQU0sRUFBRztZQUNqQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDckM7UUFDRCxPQUFPLE1BQU0sQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7Q0FJRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7UGFyYW1zQ29uZmlnVHlwZXMsIFBhcmFtc1R5cGVzLCBUeXBlc1ZhbHVlfSBmcm9tICcuL3R5cGVzJztcbmltcG9ydCB7cGFyc2VyfSBmcm9tICcuLi9oZWxwZXJzL3V0aWxzJztcblxuZXhwb3J0IGNsYXNzIFF1ZXJ5UGFyYW1EZWY8UXVlcnlQYXJhbXMgPSBhbnk+IHtcblxuICBjb25zdHJ1Y3RvciggcHJpdmF0ZSBjb25maWc6IFBhcmFtc0NvbmZpZ1R5cGVzPFF1ZXJ5UGFyYW1zPikge31cblxuXG4gIGdldCBxdWVyeUtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmNvbmZpZy5xdWVyeUtleTtcbiAgfVxuXG4gIGdldCBkZWZWYWx1ZSgpOiBUeXBlc1ZhbHVlICB7XG4gICAgcmV0dXJuIHRoaXMuY29uZmlnLmRlZlZhbHVlIHx8IHVuZGVmaW5lZDtcbiAgfVxuXG4gIGdldCB0eXBlKCk6IFBhcmFtc1R5cGVzIHtcbiAgICByZXR1cm4gdGhpcy5jb25maWcudHlwZSB8fCAnc3RyaW5nJztcbiAgfVxuXG4gIGdldCBwYXRoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuY29uZmlnLnBhdGggfHwgdGhpcy5xdWVyeUtleTtcbiAgfVxuXG4gIGdldCBwYXJzZXIoKTogKCh2YWx1ZTogc3RyaW5nKSA9PiBhbnkpIHwgdW5kZWZpbmVkIHtcbiAgICByZXR1cm4gdGhpcy5jb25maWcucGFyc2VyO1xuICB9XG5cbiAgcGFyc2UocXVlcnlQYXJhbVZhbHVlOiBzdHJpbmcpOiBhbnkge1xuICAgIGlmICggdGhpcy5wYXJzZXIgKSB7XG4gICAgICByZXR1cm4gdGhpcy5wYXJzZXIocXVlcnlQYXJhbVZhbHVlKTtcbiAgICB9XG4gICAgcmV0dXJuIHBhcnNlcihxdWVyeVBhcmFtVmFsdWUsIHRoaXMudHlwZSk7XG4gIH1cblxuXG5cbn1cbiJdfQ==