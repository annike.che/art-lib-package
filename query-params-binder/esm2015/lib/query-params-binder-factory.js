import { Injectable } from '@angular/core';
import { QueryParamsBinderManager } from './query-params-binder-manager';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
export class QueryParamsBinderFactory {
    constructor(router) {
        this.router = router;
    }
    create(params) {
        return new QueryParamsBinderManager(this.router, params, window);
    }
}
QueryParamsBinderFactory.ɵfac = function QueryParamsBinderFactory_Factory(t) { return new (t || QueryParamsBinderFactory)(i0.ɵɵinject(i1.Router)); };
QueryParamsBinderFactory.ɵprov = i0.ɵɵdefineInjectable({ token: QueryParamsBinderFactory, factory: QueryParamsBinderFactory.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(QueryParamsBinderFactory, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.Router }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXVlcnktcGFyYW1zLWJpbmRlci1mYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvcXVlcnktcGFyYW1zLWJpbmRlci9zcmMvbGliL3F1ZXJ5LXBhcmFtcy1iaW5kZXItZmFjdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRXpDLE9BQU8sRUFBQyx3QkFBd0IsRUFBQyxNQUFNLCtCQUErQixDQUFDOzs7QUFNdkUsTUFBTSxPQUFPLHdCQUF3QjtJQUNuQyxZQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUFHLENBQUM7SUFFckMsTUFBTSxDQUFLLE1BQXFEO1FBRTdELE9BQU8sSUFBSSx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNwRSxDQUFDOztnR0FOUyx3QkFBd0I7Z0VBQXhCLHdCQUF3QixXQUF4Qix3QkFBd0IsbUJBRlgsTUFBTTt1RkFFbkIsd0JBQXdCO2NBRnBDLFVBQVU7ZUFBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHtRdWVyeVBhcmFtc0JpbmRlck1hbmFnZXJ9IGZyb20gJy4vcXVlcnktcGFyYW1zLWJpbmRlci1tYW5hZ2VyJztcclxuaW1wb3J0IHtQYXJhbXNDb25maWdUeXBlc30gZnJvbSAnLi9jbGFzc2VzL3R5cGVzJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxyXG5cclxuZXhwb3J0IGNsYXNzIFF1ZXJ5UGFyYW1zQmluZGVyRmFjdG9yeSB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge31cclxuXHJcbiAgIGNyZWF0ZTxUPiggcGFyYW1zOiBQYXJhbXNDb25maWdUeXBlczxUPiB8IFBhcmFtc0NvbmZpZ1R5cGVzPFQ+W10gKTogUXVlcnlQYXJhbXNCaW5kZXJNYW5hZ2Vye1xyXG5cclxuICAgICAgcmV0dXJuIG5ldyBRdWVyeVBhcmFtc0JpbmRlck1hbmFnZXIodGhpcy5yb3V0ZXIsIHBhcmFtcywgd2luZG93KTtcclxuICAgfVxyXG59XHJcbiJdfQ==