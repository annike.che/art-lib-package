import { ɵɵinject, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject, merge } from 'rxjs';
import { set } from 'lodash';
import { debounceTime, distinctUntilChanged, map, tap, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

function parser(value, typeValue) {
    switch (typeValue) {
        case 'string':
            return value;
        case 'number':
            return +value;
        case 'boolean':
            return value === 'false' ? false : !!value;
        case 'object':
            return JSON.parse(value);
        case 'array<string>':
            return value ? value === null || value === void 0 ? void 0 : value.split(',') : undefined;
        case 'array<number>': {
            // console.log('array<number> value', value, value?.split(','));
            const result = value ? value === null || value === void 0 ? void 0 : value.split(',').map((el) => +el) : undefined;
            // console.log('array<number>', result);
            return result;
        }
        default:
            return value;
    }
}
function resolveParams(params) {
    params = transformArray(params);
    const result = {};
    params.forEach(el => {
        var _a, _b, _c;
        if (el.hasOwnProperty('defValue')) {
            // console.log(el.value, el.defValue);
            // console.log(JSON.stringify(el.value), JSON.stringify(el.defValue));
            const isChange = JSON.stringify(el.value) !== JSON.stringify(el.defValue);
            result[el.queryKey] = isChange ? ((_a = el.value) === null || _a === void 0 ? void 0 : _a.toString()) || '' : null;
        }
        else {
            // console.log('')
            const isEmpty = el.value === null || el.value === undefined || !((_b = el.value) === null || _b === void 0 ? void 0 : _b.toString());
            result[el.queryKey] = isEmpty ? null : ((_c = el.value) === null || _c === void 0 ? void 0 : _c.toString()) || '';
        }
    });
    console.log('result', result);
    return result;
}
function transformArray(value) {
    return Array.isArray(value) ? value : [value];
}

class QueryParamDef {
    constructor(config) {
        this.config = config;
    }
    get queryKey() {
        return this.config.queryKey;
    }
    get defValue() {
        return this.config.defValue || undefined;
    }
    get type() {
        return this.config.type || 'string';
    }
    get path() {
        return this.config.path || this.queryKey;
    }
    get parser() {
        return this.config.parser;
    }
    parse(queryParamValue) {
        if (this.parser) {
            return this.parser(queryParamValue);
        }
        return parser(queryParamValue, this.type);
    }
}

class QueryParamsBinderManager {
    constructor(router, defs, window) {
        this.router = router;
        this.defs = defs;
        this.window = window;
        this.group = new FormGroup({}); // группа параметров
        this.$destroy = new Subject();
        // принимаем конфиг
        this.paramsDef = transformArray(defs).map((val) => new QueryParamDef(val));
    }
    connect(group) {
        console.log('init group', group);
        this.group = group;
        this.init();
        return this;
    }
    init() {
        // обновляем контролы group из url строки
        this.updateControl(this.paramsDef, { emitEvent: true });
        // создаем массив с контролами параметров
        const controls = this.paramsDef.map(el => {
            var _a;
            return ((_a = this.group.get(el.path)) === null || _a === void 0 ? void 0 : _a.valueChanges.pipe(debounceTime(800), distinctUntilChanged(), map((value) => {
                //console.log('el', el);
                const option = {
                    value,
                    queryKey: el.queryKey,
                };
                if (el.defValue) {
                    option['defValue'] = el.defValue;
                }
                return option;
            }), tap(value => { console.log('value', value); }))) || new Subject();
        });
        /*
        * т.к. routerNavigate - это microtask,
        * нам необходимо агрегировать все изменения при помощи merge.
        * т.к. изменения могут происходить достаточно часто
        * */
        let buffer = [];
        merge(...controls)
            .pipe(
        // Получаем изменения, кладем в буффер
        map((res) => {
            console.log('map', res);
            // удаляем повторы
            const index = buffer.findIndex(el => el.queryKey === res.queryKey);
            console.log('index', index);
            if (index >= 0) {
                buffer.splice(index, 1);
            }
            return buffer.push(res);
        }), 
        // Игнорируем какое-то время и выдаем последнее(ние) изменение(ния)
        debounceTime(1000), 
        // Отписываеся, когда срабатывает subject $destroy
        takeUntil(this.$destroy))
            .subscribe(() => {
            console.log('buffer', buffer);
            // из буфера кладем в роутер только непустые параметры || != значению по умолчанию
            this.updateQueryParams(resolveParams(buffer));
            buffer = [];
        });
    }
    /*
    * Обновление query параметров в route
    * */
    updateQueryParams(params) {
        this.router.navigate([], {
            queryParams: params,
            queryParamsHandling: 'merge',
            replaceUrl: true,
        });
    }
    updateControl(defs, options, updateKey = () => true) {
        // получаем все параметры из Url строки
        const queryParams = new URLSearchParams(this.window.location.search);
        const changedParams = {};
        for (const item of defs) {
            const queryKey = item.queryKey;
            // если в URL есть параметр из присланных из модуля
            if (queryParams.has(queryKey)) {
                // берем значение из queryParams
                const newValueFromQuery = queryParams.get(queryKey);
                // парсим значение в правильный тип и вставляем в value по пути path
                set(changedParams, item.path, item.parse(newValueFromQuery));
            }
        }
        // если есть хотя бы 1 параметр на изменение
        if (Object.keys(changedParams).length) {
            this.group.patchValue(changedParams, options);
            console.log('group', this.group);
            console.log('defs', this.defs, defs);
        }
    }
    destroy() {
        this.$destroy.next();
    }
}

class QueryParamsBinderFactory {
    constructor(router) {
        this.router = router;
    }
    create(params) {
        return new QueryParamsBinderManager(this.router, params, window);
    }
}
QueryParamsBinderFactory.ɵfac = function QueryParamsBinderFactory_Factory(t) { return new (t || QueryParamsBinderFactory)(ɵɵinject(Router)); };
QueryParamsBinderFactory.ɵprov = ɵɵdefineInjectable({ token: QueryParamsBinderFactory, factory: QueryParamsBinderFactory.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(QueryParamsBinderFactory, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: Router }]; }, null); })();

/*
 * Public API Surface of query-params-binder
 */

/**
 * Generated bundle index. Do not edit.
 */

export { QueryParamsBinderFactory };
//# sourceMappingURL=art-lib-query-params-binder.js.map
