import { ParamsConfigTypes, ParamsTypes, TypesValue } from './types';
export declare class QueryParamDef<QueryParams = any> {
    private config;
    constructor(config: ParamsConfigTypes<QueryParams>);
    get queryKey(): string;
    get defValue(): TypesValue;
    get type(): ParamsTypes;
    get path(): string;
    get parser(): ((value: string) => any) | undefined;
    parse(queryParamValue: string): any;
}
//# sourceMappingURL=QueryParamDef.d.ts.map