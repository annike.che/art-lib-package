export declare type ParamsTypes = 'boolean' | 'number' | 'string' | 'object' | 'array<string>' | 'array<number>';
export declare type TypesValue = boolean | number | string | object | Array<string> | Array<number> | undefined;
export declare type ParamsConfigTypes<QueryParams = any> = {
    queryKey: keyof QueryParams & string;
    defValue?: TypesValue;
    path?: string;
    type?: ParamsTypes;
    strategy?: 'modelToUrl' | 'twoWay';
    parser?: (value: string) => any;
};
//# sourceMappingURL=types.d.ts.map