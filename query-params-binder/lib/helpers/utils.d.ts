import { ParamsTypes } from '../classes/types';
export declare function parser(value: any, typeValue: ParamsTypes): any;
export declare function resolveParams(params: ResolveParamsOption[] | ResolveParamsOption): any;
export declare function transformArray<T>(value: T | T[]): T[];
export interface ResolveParamsOption {
    value: any;
    queryKey: string;
    defValue?: any;
}
//# sourceMappingURL=utils.d.ts.map