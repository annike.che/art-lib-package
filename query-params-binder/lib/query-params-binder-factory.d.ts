import { Router } from '@angular/router';
import { QueryParamsBinderManager } from './query-params-binder-manager';
import { ParamsConfigTypes } from './classes/types';
import * as i0 from "@angular/core";
export declare class QueryParamsBinderFactory {
    private router;
    constructor(router: Router);
    create<T>(params: ParamsConfigTypes<T> | ParamsConfigTypes<T>[]): QueryParamsBinderManager;
    static ɵfac: i0.ɵɵFactoryDef<QueryParamsBinderFactory, never>;
    static ɵprov: i0.ɵɵInjectableDef<QueryParamsBinderFactory>;
}
//# sourceMappingURL=query-params-binder-factory.d.ts.map