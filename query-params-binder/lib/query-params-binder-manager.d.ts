import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ParamsConfigTypes } from './classes/types';
export declare class QueryParamsBinderManager<T = any> {
    private router;
    private defs;
    private window;
    private paramsDef;
    private group;
    private $destroy;
    constructor(router: Router, defs: ParamsConfigTypes<T> | ParamsConfigTypes<T>[], window: Window);
    connect(group: FormGroup): any;
    init(): void;
    private updateQueryParams;
    private updateControl;
    destroy(): void;
}
//# sourceMappingURL=query-params-binder-manager.d.ts.map